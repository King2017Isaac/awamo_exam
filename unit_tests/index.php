<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Exam Tests</title>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://code.jquery.com/qunit/qunit-2.6.1.css">

  <script src="https://code.jquery.com/qunit/qunit-2.6.1.js"></script>
  <script src="../js/mock-backend.js"></script>
  <script src="../js/main.js"></script>
 <script src="test_compute.js"></script>
 <script src="test_api.js"></script>
</head>
<body>
  <div id="qunit"></div>
  <div id="qunit-fixture"></div>
  
</body>
</html>