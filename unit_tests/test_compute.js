
function calculate_value1( num1, num2, operation  ) {
    var expected;

    switch( operation ) {
        case 'ADD':
            expected = num1 + num2;
            break;
        case 'SUB':
            expected = num1 - num2;
            break;
        case 'MUL':
            expected = num1 * num2;
            break;
        case 'DIV':
            expected = ( num1 / num2 );
            break;
        default:
            expected = '';
            break;
    }

    return expected;
}
//unit test for the function that computes the accurate value 
 
QUnit.test('calculate_value1()', function( assert ) {
  assert.ok( calculate_value1(1,2, 'SEKA' , '' ) , 'Wrong operation given' );
  assert.ok( calculate_value1(1,'', '' , '' ) , 'One empty parameter' );
  assert.ok( calculate_value1('','', 'ADD' , '' ) , '2 empty parameters' );
  assert.ok( calculate_value1(2,9, 'ADD' , '' ) , 'Add' );
  assert.ok( calculate_value1(2,1, 'SUB' , '' ) , 'Subtract' );
  assert.ok( calculate_value1(3,10, 'DIV', '' ) , 'Division' );
  assert.ok( calculate_value1(4,5, 'MUL' , '' ) , 'Product' );
  assert.ok( calculate_value1(40000,50, 'MUL' , '' ) , 'Big numbers product' );
  
});







