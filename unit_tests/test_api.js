//unit testing the api

function call_api( data_object ) {
    $.ajax( {
        url: '#',
        type: 'POST',
        data: JSON.stringify( data_object ),
        success: function( response ){
            console.log(response);
        },
        error: function() {
            console.log('fail');
        }
    }  );
}

function parse_n_stringify( object1 ) {
    var x = JSON.parse( JSON.stringify( object1 ) );
    return x;
}

QUnit.test('parse_n_stringify()', function( assert ) {
  assert.ok( parse_n_stringify( { "opd1":4, "opd2":3, "opn":'ADD' } ), 'Testing JSON object conversion to string' );
  assert.ok( parse_n_stringify( { "opd1":4, "opd2":"Test", "opn":'SUB' } ), 'Testing JSON object conversion to string' );
  assert.ok( parse_n_stringify( { "opd1":"", "opd2":"", "opn":'ADD' } ), 'Testing JSON object conversion to string' );
  assert.ok( parse_n_stringify( { 'opd1':'3', "opd2":'9.5', "opn":'ADDITION'  } ), 'Testing JSON object conversion to string' );
  
});




QUnit.test('call_api()', function( assert ) {
    assert.ok( call_api( { "opd1":4, "opd2":3, "opn":'ADD' } ), 'Send ADD' );
    assert.ok( call_api( { "opd1":4, "opd2":"Test", "opn":'SUB' } ), 'Send SUB' );
    assert.ok( call_api( { "opd1":"", "opd2":"", "opn":'ADD' } ), 'Send ADD with an empty' );
    assert.ok( call_api( { 'opd1':'3', "opd2":'9.5', "opn":'ADDITION' } ), 'Send ADDITION' );
    
  });