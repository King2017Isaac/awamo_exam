<!DOCTYPE html>
    <html>
        <head>
            <title> Awamo Exam v2 </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
            <link href="css/exam.css" rel="stylesheet" />

            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
            <script src="js/mock-backend.js"></script>
            <script src="js/main.js"></script>
        </head>
        <body style="background-color: #">
        
            <div class="container2">
                <div class="sidebar">
                    <form action="#" id="form1">
                        <h4 class="sidebar_heading"><b>Numbers</b></h4>
                        <label>Number one</label><input class="form-control" type="text" onkeyup="this.value = this.value.replace(/[^\d\.\,\d]+/g, '');" name="number_1"  placeholder="Enter number one" id="number_1" autocomplete="off" required >
                        <label>Number two</label><input class="form-control" type="text" onkeyup="this.value = this.value.replace(/[^\d\.\,\d]+/g, '');" name="number_2"  placeholder="Enter number two" id="number_2" autocomplete="off" required >
                        <label>Operation</label>
                        <select class="form-control select1" name="operation" id="operation" required>
                            <option value="ADD"> Add </option>
                            <option value="SUB"> Subtract </option>
                            <option value="MUL"> Multiply </option>
                            <option value="DIV"> Divide </option>
                        </select>
                        <input class="form-control" type="submit" id="submit" value="Post" name="submit" >
                    </form>
                </div>
                <div class="tablecontainer">
                    <h4 id='table_heading'> <b>Results</b> </h4>
                    <div class="custom-table">
                        <table class="table">
                            <thead class="header">
                                <tr>
                                    <th>Number 1</th>
                                    <th>Number 2</th>
                                    <th>Response</th>
                                    <th>Expected</th>
                                    <th>Passed</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </body>
    </html>