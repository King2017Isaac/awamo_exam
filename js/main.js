$( function() {
    
    $( '#form1' ).on( 'submit', function( e ) {

        //prevent page from submitting
        e.preventDefault();
        
        //pick values from html form
        var num1 = parseInt( $( '#number_1' ).val() ),
            num2 = parseInt( $( '#number_2' ).val() ),
            operation = $( '#operation' ).val();

        //define more variables
        var expected  = calculate_value( num1, num2, operation ), 
                passed,
                data_object,
                markup = '';
			
			
            //create an object of the data
            data_object = { "opd1":num1, "opd2":num2, "opn":operation };

            //call the api
            $.ajax({
                url: '#',
                type: 'POST',
                data: JSON.stringify( data_object ),
                success: function( response ){
                    //compare values returned from API and computed from here  
                    passed = ( expected == response ) ? 'Yes': 'No';

                   
                    remove = "<img src='img/close1.png' width='10' height='10' alt='close' onclick='deleteRow(this)'/>";

                    //build table rows on each form submit
                    if( passed == 'No' ){
                        markup = "<tr style='background:#ffaea5;text-align:center;'> <td style='border-bottom:1pt solid black;'>" + num1 + "</td> <td style='border-bottom:1pt solid black;'>" + num2 + "</td> <td style='border-bottom:1pt solid black;'>" + response  + "</td> <td style='border-bottom:1pt solid black;'>" + expected + "</td> <td style='border-bottom:1pt solid black;'>" + passed + "</td> <td style='border-bottom:1pt solid black;'>" + remove + "</td> </tr>";
                    }
                    else {
                        markup = "<tr style='background:#ffe08a;text-align:center;'> <td style='border-bottom:1pt solid black;'>" + num1 + "</td> <td style='border-bottom:1pt solid black;'>" + num2 + "</td> <td style='border-bottom:1pt solid black;'>" + response + "</td> <td style='border-bottom:1pt solid black;'>" + expected + "</td> <td style='border-bottom:1pt solid black;'>" + passed + "</td> <td style='border-bottom:1pt solid black;'>" + remove + "</td> </tr>";
                    }
                    
                    
                    $("table tbody").append(markup);
                    
                },
                error: function(error ){
                 console.log( 'error occurred'  );
                }
              });

        
        
    } );
} );


function calculate_value( num1, num2, operation  ) {
    var expected;

    switch( operation ) {
        case 'ADD':
            expected = num1 + num2;
            break;
        case 'SUB':
            expected = num1 - num2;
            break;
        case 'MUL':
            expected = num1 * num2;
            break;
        case 'DIV':
            expected = ( num1 / num2 );
            break;
        default:
            expected = '';
            break;
    }

    return expected;
}


function deleteRow( btn ) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
  }